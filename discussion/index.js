// JS Synchronous vs Asunchronous
/*
- default behavior ni JS ay synchronous (from top to bottom)

*/



// console.log("hello world");
// console.log("goodbye");

// for (let i=1; i<=1500; i++){
// 	console.log(i);
// };
// console.log("it's me again");
// when statements take time to process, this slows down our code. lalo pag malaking amount of info or when fetching data from database

// Fetch API - allows us to asynchronous request for a resourse (data);
// Asynchronous - allows executing of codes simultaneously prioritizing on the less resource-consuming codes while the more complex and more resource-consuming codes run at the back
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
/*
Syntax:
	fetch(URL);

*/
// console.log(fetch("https://jsonplaceholder.typicode.com/posts/"));
// console.log("Hello");


// Section - Aysnchronous
// Fetch:
// para mauna yung console.log:
// retreves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts/")
// by using the ".then" method, we can now check the status of the promise
// "fetch" method will return a "promise" that resolves to a response object
// the ".then" method captures the "response" object and returns another "promise" which will eventually be resolved or rejected
// .then(response=>console.log(response.status));


// console.log("Hello");

// try: magrereturn ito sa console ng arrays pero puro lorem ipsum
// we use the "json" method from the response object to convert the data retrieved into JSON format to be used in our applic. pag di mo iconvert to JSON, mag-error 
.then(response=>response.json())
// using multiple "then" methods will result into "promise chain"
// then we can now access it and log in the console our desired output
.then(json=>console.log(json));
console.log("hello");
console.log("hello world");
console.log("hello again");
// pag ganito, sa console, uunahin ilog itong tatlong messages (so asynchronous na siya) kasi mas madali yun kesa magfetch siya ng data kaya inuna na ni JS

// Async-Await
// "async" and "await" keywords are other approach that can be used to perform asynchronous javascript 
// used in functions to indicate which portions of the code should be waited for

async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/posts/");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json=await result.json();
	console.log(json);
};
fetchData();
console.log("Hello");

// Section- Creating a post
/*
Syntax - 
	fetch(URL, options)
*/
// create a new post following the REST IP (create, posts/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts/", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	// set content/body data of the "request" object to be sent to the backend 
	// JSON. stringify converts the object into a stringified JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})

})
.then(response=>response.json())
.then(json=>console.log(json))
// Sa postman ito ni-run since POST method siya, at di naman nakikita sa console pag post, update, delete
// make sure raw, JSON pag mag-request sa postman

// Section - updating a post (PUT)
// updates a specific post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello again",
		userId: 1
	})

})
.then(response=>response.json())
.then(json=>console.log(json));

// Section- update a post(PATCH)
/*
The difference between PUT and PATCH is the number of porperties being updated. PATCH is used to update a single property while maintaining the unupdated properties
PUT is used when all of the properties
*/
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post",
	})

})
.then(response=>response.json())
.then(json=>console.log(json));

// Section- deleting
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})

// Section - Filter posts
fetch("https://jsonplaceholder.typicode.com/posts?userID=1")
.then(response=>response.json())
.then(json=>console.log(json));

// the data can be filtered by sending the userId along with the URL
// information sent via the URL can be done by adding the ? symbol - kung ano yung hinahanap mo
/*
Syntax:
	"<url>?parameterName=value"
	"<url>?parameterName=valueA&&parameterNameB=valueB" for multiple parameter search

*/

 //Retrieving comments for a specific post/accessing nested/embedded comments
  fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response=>response.json())
.then(json=>console.log(json));