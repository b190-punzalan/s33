

// https://jsonplaceholder.typicode.com/todos
console.log("Activity Time!");

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response=>response.json())
.then(json=>console.log(json))
// console.log("hello");

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


// let toDos = [fetch("https://jsonplaceholder.typicode.com/todos")];
// let title = toDos.map(function(title){
// })
// console.log("Result of map: " );
// console.log(toDosMap);





// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response=>response.json())
.then(json=>console.log(json))

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos?title=1&&status=1")
.then(response=>response.json())
.then(json=>console.log(json));


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	
	body: JSON.stringify({
		id: 201,
		title: "Created To Do List Item",
		completed: false,
		userId: 1
	})

})
.then(response=>response.json())
.then(json=>console.log(json))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-type": "application/json"
	},
	
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list",
		id: 1,
		title: "Updated To Do List",
		status: "Pending",
		userId: 1	
	})

})
.then(response=>response.json())
.then(json=>console.log(json))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-type": "application/json"
	},
	
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		title: "delectus aut autem",
		status: "Complete",
	})

})
.then(response=>response.json())
.then(json=>console.log(json))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})
